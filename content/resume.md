---
title: "Resume"
date: 2022-06-09T12:27:53-04:00
draft: false
---

#### Experience

---

**Vmware | Raleigh, NC**

**Product Support Engineer (Intern) | 05/2021 - 08/2021, 01/2022 - present**

* Triaged critical/major issues occuring on VMware Tanzu Platform. 

* Developed internal tools for team using Golang.

* Resolved issues with Spring, MySQL, Concourse, Grafana, and more. 

#### Projects

---

- Wordle App
  
  - Wordle game web app built with Spring Boot.
  
  - MySQL backing database with JavaScript, HTML, and CSS frontend.
* PathFinder
  
  * IOT device to get GPS directions using Raspberry Pi and Python.
  
  * MQTT protocol to communicate between end user and device.

* Embedded Systems
  
  * Robot using MSP-430 microcontroller that follows line and receives instructions.
  
  * Used C language to program interrupts, subroutines, etc.

#### Skills

---

C/C++, Java, Python, Networks, Linux, OOP, Spring Boot, Wireshark, Golang, VMware, Docker

#### Education

---

**North Carolina State University | Raleigh, NC**

**B.S. Computer Engineering | 08/2022**

Studied computer architecture, compilers, data structures, networks, and parallel computing architecture

**North Carolina State University | Raleigh, NC**

**B.S. Electrical Engineering | 08/2022**

Studied fundamentals of microelectronics, robotic systems, and controls
